;;;;
;; Sage
;;;;

;;(use-package highlight-indentation
 ;;  :ensure t)


;; Run SageMath by M-x run-sage instead of M-x sage-shell:run-sage
(sage-shell:define-alias)

;; Turn on eldoc-mode in Sage terminal and in Sage source files
(add-hook 'sage-shell-mode-hook #'eldoc-mode)
(add-hook 'sage-shell:sage-mode-hook #'eldoc-mode)

(setq sage-shell:use-simple-prompt nil)

;; Turn on highlight indentation
(add-hook 'sage-mode-hook 'highlight-indentation-mode)
(add-hook 'sage-mode-hook 'highlight-indentation-current-column-mode)
(setq highlight-indentation-blank-lines t)

;; Turn on anaconda mode
(add-hook 'sage-mode-hook 'anaconda-mode)

;; Point sage to binary
(setq sage-shell:sage-executable "/usr/bin/sage")

(add-hook 'sage-mode-hook 'sphinx-doc-mode)

(setq sphinx-doc-include-types t)

(dolist (ckr '(python-pylint python-flake8))
  (flycheck-add-mode ckr 'sage-shell:sage-mode))

(defun sage-shell:flycheck-turn-on ()
  "Enable flycheck-mode only in a file ended with py."
  (when (let ((bfn (buffer-file-name)))
          (and bfn (string-match (rx ".py" eol) bfn)))
    (flycheck-mode 1)))

(add-hook 'python-mode-hook 'sage-shell:flycheck-turn-on)
