;;;;
;; Org-Mode
;;;;

(setq org-babel-clojure-backend 'cider)

(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;; enable word wrap
(add-hook 'org-mode-hook #'toggle-word-wrap)

;;LaTeX Export
