(defun random-alnum ()
  (let* ((alnum "abcdefghijklmnopqrstuvwxyz0123456789")
         (i (% (abs (random)) (length alnum))))
    (substring alnum i (1+ i))))

(defun random-5-letter-string ()
  (interactive)
  (concat 
   (random-alnum)
   (random-alnum)
   (random-alnum)
   (random-alnum)
   (random-alnum)))

(defun insert-latex-image ()
  "Create SVG and insert the link"
  (interactive)
  ;(message default-directory)
  (setq latexsnippet "\n
\\begin{figure}[htb] \n
  \\centering \n
  \\includegraphics{%s} \n
\\end{figure}")
  (setq filename (concat default-directory "Images/figure-" 
                     (random-5-letter-string) 
                     ".svg"))
  (copy-file "~/.customdesktopconfig/configs/inkscape/template.svg" filename)
  (shell-command (format "inkscape %s" filename))
  (shell-command-to-string 
   (format "inkscape ./Images/%s --export-pdf=./Images/%s --export-area-drawing --export-latex" 
           (file-name-nondirectory filename)
           (concat (file-name-base filename) ".pdf")))
  (insert (format latexsnippet (file-name-base filename))))


(defun open-latex-image ()
  "print the word under cursor.
Word here is any A to Z, a to z, and low line _"
  (interactive)
  (let (
        p1
        p2
        (case-fold-search t))
    (save-excursion
      (skip-chars-backward "-_a-z0-9" )
      (setq p1 (point))
      (skip-chars-forward "-_a-z0-9" )
      (setq p2 (point))
      (setq filename (buffer-substring-no-properties p1 p2))))
  (shell-command (format "inkscape ./Images/%s" (concat filename ".svg")))
  (shell-command-to-string 
   (format "inkscape ./Images/%s --export-pdf=./Images/%s --export-area-drawing --export-latex" 
           (concat (file-name-nondirectory filename) ".svg")
           (concat (file-name-base filename) ".pdf"))))
