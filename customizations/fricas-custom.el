(defun fricas-run ()
  "Run FriCAS in the current BUFFER."
  (message "Starting FriCAS...")
  (start-process-shell-command "fricas" (current-buffer)
                               fricas-run-command
                               "-noclef" "2>/dev/null"))
