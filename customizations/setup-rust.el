;;;;
;; Rust
;;;;

;; original config from here :
;; http://julienblanchard.com/2016/fancy-rust-development-with-emacs/

;; Allow us to run rust commands from emacs
(add-hook 'rust-mode-hook 'cargo-minor-mode)

;; requires rustfmt
;; allows you to C-c <tab> to auto indent current buffer
(add-hook 'rust-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c <tab>") #'rust-format-buffer)))

(defun indent-buffer ()
  "Indent current buffer according to major mode."
  (interactive)
  (indent-region (point-min) (point-max)))

;; requires racer
(setq racer-cmd "~/.cargo/bin/racer")
(setq racer-rust-src-path "~/rusc-1.11.0/src")

(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)

(setq company-tooltip-align-annotations t)

(add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
